import { Component, OnInit } from '@angular/core';
import { DataService } from './data.service';
import { Recipe } from './recipe';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  //title = 'app';
  
  recipes: Recipe[];

  constructor(private dataService: DataService) {

  }

  ngOnInit() {
    // slået fra fordi dataservice er lavet om til http
  	//this.recipes = this.dataService.getRecipes();
  }

  /*recipes = [
	  {
	  	id:1,
	  	name:'strwbaerry tart 22',
	  	ingredients: ['100g butter','200g strawberry','300 flour']
	  },
	  {
	  	id:2,
	  	name:'blueberry tart',
	  	ingredients: ['100g butter','200g sdfadss','300 flour']
	  },
	  {
	  	id:3,
	  	name:'pickle tart',
	  	ingredients: ['100g butter','200g pickles','300 flour']
	  }
  ];

  logClick() {
  	alert('hejsa');
  }

  showLength(event) {
  	console.log(event.target.value);
  }
  */
}

