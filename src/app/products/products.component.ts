import { Component, OnInit, Input } from '@angular/core';
import { Product } from '../product';
import { DataService } from '../data.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {
	@Input() search:string;
	@Input() max:string;

	products:Product[];

	antal = 0;

  constructor(private dataService: DataService) { }

  ngOnInit() {
  	this.products = this.dataService.getProducts();
    //this.dataService.getProducts2().then(products => this.products = products);
  }

  searchProducts(search,max) {
  	//console.log(search);
  	return this.products.sort(eval('this.'+search)).slice(0,max);
  }

  all(a,b) {
  	if ( a.id > b.id ) return -1;
  	else return 1;
  }

  newArrivals(a,b) {
  	if ( a.entered > b.entered ) return -1;
  	else return 1;
  }

  topSellers(a,b) {
  	if ( a.items_sold > b.items_sold ) return -1;
  	else return 1;
  }

  featured(a,b) {
  	if ( a.featured ) return -1;
  	else return 1;
  }

}


/*
  searchProducts(search) {
  	//this.antal = 0;
  	//return this.products.slice(0,4);
  	//return this.products.filter(eval('this.'+search),this);
  	eval('this.'+search);
  }

  newArrivals(product) {
  	if ( this.antal++ > 3 ) return false; 
  	else return true;
  }

  all(product) {
  	return true;
  }

  topSellers(product) {
  	return true;
  }

  featured(product) {
  	return true;
  }
*/