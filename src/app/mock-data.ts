import { Recipe } from './recipe';

export const recipeData:Recipe[] =
[
      {  
         "is_featured":false,
         "publisher":"The Pioneer Woman",
         "f2f_url":"http://food2fork.com/view/47024",
         "ingredients":[  
            "1 pound Ground Coffee (good, Rich Roast)",
            "8 quarts Cold Water",
            "Half-and-half (healthy Splash Per Serving)",
            "Sweetened Condensed Milk (2-3 Tablespoons Per Serving)",
            "Note: Can Use Skim Milk, 2% Milk, Whole Milk, Sugar, Artificial Sweeteners, Syrups...adapt To Your Liking!"
         ],
         "source_url":"http://thepioneerwoman.com/cooking/2011/06/perfect-iced-coffee/",
         "_id":"47024",
         "image_url":"http://static.food2fork.com/icedcoffee5766.jpg",
         "social_rank":100,
         "publisher_url":"http://thepioneerwoman.com",
         "title":"Perfect Iced Coffee",
         "reviews":[  
            {  
               "name":"Timmy",
               "email":"timmy@abc.com",
               "comment":"Was yummy, very refreshing",
               "posted":new Date("2016-01-02"),
               "rating":5
            },
            {  
               "name":"Bobby",
               "email":"bob@test.com",
               "comment":"Was missing a bit of sweetness",
               "posted":new Date("2016-02-02"),
               "rating":3
            }
         ]
      },
      {  
         "is_featured":true,
         "publisher":"Closet Cooking",
         "f2f_url":"http://food2fork.com/view/35382",
         "ingredients":[  
            "2 jalapeno peppers, cut in half lengthwise and seeded",
            "2 slices sour dough bread",
            "1 tablespoon butter, room temperature",
            "2 tablespoons cream cheese, room temperature",
            "1/2 cup jack and cheddar cheese, shredded",
            "1 tablespoon tortilla chips, crumbled\n"
         ],
         "source_url":"http://www.closetcooking.com/2011/04/jalapeno-popper-grilled-cheese-sandwich.html",
         "_id":"35382",
         "image_url":"http://static.food2fork.com/Jalapeno2BPopper2BGrilled2BCheese2BSandwich2B12B500fd186186.jpg",
         "social_rank":100,
         "publisher_url":"http://closetcooking.com",
         "title":"Jalapeno Popper Grilled Cheese Sandwich"
      },
      {  
         "is_featured":true,
         "publisher":"The Pioneer Woman",
         "f2f_url":"http://food2fork.com/view/47319",
         "ingredients":[  
            "12 whole New Potatoes (or Other Small Round Potatoes)",
            "3 Tablespoons Olive Oil",
            "Kosher Salt To Taste",
            "Black Pepper To Taste",
            "Rosemary (or Other Herbs Of Choice) To Taste"
         ],
         "source_url":"http://thepioneerwoman.com/cooking/2008/06/crash-hot-potatoes/",
         "_id":"47319",
         "image_url":"http://static.food2fork.com/CrashHotPotatoes5736.jpg",
         "social_rank":100,
         "publisher_url":"http://thepioneerwoman.com",
         "title":"Crash Hot Potatoes",
         "reviews":[  
            {  
               "name":"Jimmy",
               "email":"jimm@xyx.com",
               "comment":"What can go wrong with potatoes. Very tasty",
               "posted":new Date("2016-03-02"),
               "rating":5
            }
         ]
      },
      {  
         "is_featured":true,
         "publisher":"Two Peas and Their Pod",
         "f2f_url":"http://food2fork.com/view/54384",
         "ingredients":[  
            "10 ounces dry elbow macaroni",
            "2 cloves garlic, minced",
            "2 avocados, peeled and pitted",
            "2 tablespoons fresh lime juice",
            "1/3 cup chopped fresh cilantro",
            "Salt and pepper, to taste",
            "2 tablespoons butter",
            "2 tablespoons all-purpose flour",
            "1 cup milk",
            "2 cups shredded Pepper Jack cheese",
            "Salt and pepper, to taste",
            "Fresh avocado chunks, for garnish, if desired"
         ],
         "source_url":"http://www.twopeasandtheirpod.com/stovetop-avocado-mac-and-cheese/",
         "_id":"54384",
         "image_url":"http://static.food2fork.com/avocadomacandcheesedc99.jpg",
         "social_rank":100,
         "publisher_url":"http://www.twopeasandtheirpod.com",
         "title":"Stovetop Avocado Mac and Cheese"
      },
      {  
         "is_featured":true,
         "publisher":"Closet Cooking",
         "f2f_url":"http://food2fork.com/view/35171",
         "ingredients":[  
            "1/4 cup cooked shredded chicken, warm",
            "1 tablespoon hot sauce",
            "1/2 tablespoon mayo (optional)",
            "1 tablespoon carrot, grated",
            "1 tablespoon celery, sliced",
            "1 tablespoon green or red onion, sliced or diced",
            "1 tablespoon blue cheese, room temperature, crumbled",
            "1/2 cup cheddar cheese, room temperature, grated",
            "2 slices bread",
            "1 tablespoon butter, room temperature\n"
         ],
         "source_url":"http://www.closetcooking.com/2011/08/buffalo-chicken-grilled-cheese-sandwich.html",
         "_id":"35171",
         "image_url":"http://static.food2fork.com/Buffalo2BChicken2BGrilled2BCheese2BSandwich2B5002B4983f2702fe4.jpg",
         "social_rank":100,
         "publisher_url":"http://closetcooking.com",
         "title":"Buffalo Chicken Grilled Cheese Sandwich",
         "reviews":[  
            {  
               "name":"Jane",
               "email":"jane@eal.com",
               "comment":"Not my favourite. Didn't taste of anything",
               "posted":new Date("2016-04-02"),
               "rating":2
            },
            {  
               "name":"Fred",
               "email":"fred@eal.com",
               "comment":"So so, I wouldn't make it again.",
               "posted":new Date("2016-04-05"),
               "rating":3
            }
         ]
      },
      {  
         "is_featured":true,
         "publisher":"The Pioneer Woman",
         "f2f_url":"http://food2fork.com/view/d9a5e8",
         "ingredients":[  
            "1 quart Whole Milk",
            "1 cup Vegetable Oil",
            "1 cup Sugar",
            "2 packages Active Dry Yeast, 0.25 Ounce Packets",
            "8 cups (Plus 1 Cup Extra, Separated) All-purpose Flour",
            "1 teaspoon (heaping) Baking Powder",
            "1 teaspoon (scant) Baking Soda",
            "1 Tablespoon (heaping) Salt",
            "Plenty Of Melted Butter",
            "2 cups Sugar",
            "Generous Sprinkling Of Cinnamon",
            "_____",
            "MAPLE FROSTING:",
            "1 bag Powdered Sugar",
            "2 teaspoons Maple Flavoring",
            "1/2 cup Milk",
            "1/4 cup Melted Butter",
            "1/4 cup Brewed Coffee",
            "1/8 teaspoon Salt"
         ],
         "source_url":"http://thepioneerwoman.com/cooking/2007/06/cinammon_rolls_/",
         "_id":"d9a5e8",
         "image_url":"http://static.food2fork.com/333323997_04bd8d6c53da11.jpg",
         "social_rank":100,
         "publisher_url":"http://thepioneerwoman.com",
         "title":"Cinnamon Rolls",
         "reviews":[  
            {  
               "name":"Jane",
               "email":"jane@eal.com",
               "comment":"Delicious!",
               "posted":new Date("2016-04-04"),
               "rating":5
            },
            {  
               "name":"Fred",
               "email":"fred@eal.com",
               "comment":"Best for breakfast",
               "posted":new Date("2016-03-05"),
               "rating":5
            }
         ]
      },
      {  
         "is_featured":true,
         "publisher":"101 Cookbooks",
         "f2f_url":"http://food2fork.com/view/47746",
         "ingredients":[  
            "4 1/2 cups (20.25 ounces) unbleached high-gluten, bread, or all-purpose flour, chilled",
            "1 3/4 (.44 ounce) teaspoons salt",
            "1 teaspoon (.11 ounce) instant yeast",
            "1/4 cup (2 ounces) olive oil (optional)",
            "1 3/4 cups (14 ounces) water, ice cold (40F)",
            "Semolina flour OR cornmeal for dusting"
         ],
         "source_url":"http://www.101cookbooks.com/archives/001199.html",
         "_id":"47746",
         "image_url":"http://static.food2fork.com/best_pizza_dough_recipe1b20.jpg",
         "social_rank":100,
         "publisher_url":"http://www.101cookbooks.com",
         "title":"Best Pizza Dough Ever"
      },
      {  
         "is_featured":true,
         "publisher":"101 Cookbooks",
         "f2f_url":"http://food2fork.com/view/47899",
         "ingredients":[  
            "1/2 cup extra-virgin olive oil",
            "1 teaspoon fresh rosemary leaves",
            "1 teaspoon fresh thyme leaves",
            "1 teaspoon fresh oregano leaves",
            "2 teaspoons sweet paprika",
            "2 medium cloves of garlic, smashed into a paste",
            "1 well-crumbled bay leaf",
            "pinch of red pepper flakes",
            "1/4 teaspoon + fine grain sea salt",
            "1 tablespoon fresh lemon juice"
         ],
         "source_url":"http://www.101cookbooks.com/archives/magic-sauce-recipe.html",
         "_id":"47899",
         "image_url":"http://static.food2fork.com/magic_sauce_recipeece9.jpg",
         "social_rank":100,
         "publisher_url":"http://www.101cookbooks.com",
         "title":"Magic Sauce"
      },
      {  
         "is_featured":true,
         "publisher":"The Pioneer Woman",
         "f2f_url":"http://food2fork.com/view/47042",
         "ingredients":[  
            "1 whole Large Onion",
            "1 whole Pork Shoulder (\"pork Butt\") - 5 To 7 Pounds",
            "Salt And Freshly Ground Black Pepper",
            "1 can (11 Ounce) Chipotle Peppers In Adobo Sauce",
            "2 cans Dr. Pepper",
            "2 Tablespoons Brown Sugar"
         ],
         "source_url":"http://thepioneerwoman.com/cooking/2011/03/spicy-dr-pepper-shredded-pork/",
         "_id":"47042",
         "image_url":"http://static.food2fork.com/5551711173_dc42f7fc4b_zbd8a.jpg",
         "social_rank":100,
         "publisher_url":"http://thepioneerwoman.com",
         "title":"Spicy Dr. Pepper Shredded Pork"
      },
      {  
         "is_featured":true,
         "publisher":"Whats Gaby Cooking",
         "f2f_url":"http://food2fork.com/view/713134",
         "ingredients":[  
            "4 cups cubed Yukon Gold potatoes",
            "3 tbsp olive oil",
            "1/2 tsp garlic salt",
            "1/2 tsp salt",
            "2 tsp paprika",
            "1 tsp pepper",
            "4 tablespoons freshly grated Parmesan cheese",
            "InstructionsPreheat your oven to 425 degrees."
         ],
         "source_url":"http://whatsgabycooking.com/parmesan-roasted-potatoes/",
         "_id":"713134",
         "image_url":"http://static.food2fork.com/ParmesanRoastedPotatoes11985a.jpg",
         "social_rank":100,
         "publisher_url":"http://whatsgabycooking.com",
         "title":"Parmesan Roasted Potatoes"
      }
];
