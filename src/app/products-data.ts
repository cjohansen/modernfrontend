import { Product } from './product';

export const products:Product[] = [
	{
		'id':1,
		'description':'crochet back playsuit',
		'price':'47.10',
		'image':'product1.png',
		'sale':true,
		'entered':new Date('2017-01-01'),
		'featured':true,
		'items_sold':10
	},
	{
		'id':2,
		'description':'aztec printed jumpsuit',
		'price':'54.20',
		'image':'product2.png',
		'sale':false,
		'entered':new Date('2017-01-12'),
		'featured':true,
		'items_sold':5
	},
	{
		'id':3,
		'description':'aztec printed jumpsuit',
		'price':'54.20',
		'image':'product3.png',
		'sale':false,
		'entered':new Date('2017-01-14'),
		'featured':true,
		'items_sold':2
	},
	{
		'id':4,
		'description':'aztec printed jumpsuit',
		'price':'54.20',
		'image':'product4.png',
		'sale':true,
		'entered':new Date('2017-01-23'),
		'featured':true,
		'items_sold':0
	},
	{
		'id':5,
		'description':'aztec printed jumpsuit',
		'price':'54.20',
		'image':'product5.png',
		'sale':true,
		'entered':new Date('2017-01-05'),
		'featured':false,
		'items_sold':1
	},
	{
		'id':6,
		'description':'aztec printed jumpsuit',
		'price':'54.20',
		'image':'product6.png',
		'sale':false,
		'entered':new Date('2017-01-10'),
		'featured':true,
		'items_sold':1
	},
	{
		'id':7,
		'description':'aztec printed jumpsuit',
		'price':'54.20',
		'image':'product7.png',
		'sale':false,
		'entered':new Date('2017-01-02'),
		'featured':true,
		'items_sold':4
	},
	{
		'id':8,
		'description':'aztec printed jumpsuit',
		'price':'54.20',
		'image':'product8.png',
		'sale':false,
		'entered':new Date('2017-01-15'),
		'featured':false,
		'items_sold':7
	},
	{
		'id':9,
		'description':'aztec printed jumpsuit',
		'price':'54.20',
		'image':'product9.png',
		'sale':false,
		'entered':new Date('2017-01-28'),
		'featured':false,
		'items_sold':5
	},
	{
		'id':10,
		'description':'aztec printed jumpsuit',
		'price':'54.20',
		'image':'product10.png',
		'sale':true,
		'entered':new Date('2017-01-27'),
		'featured':false,
		'items_sold':3
	},
	{
		'id':11,
		'description':'aztec printed jumpsuit',
		'price':'54.20',
		'image':'product11.png',
		'sale':false,
		'entered':new Date('2017-01-15'),
		'featured':true,
		'items_sold':1
	},
	{
		'id':12,
		'description':'aztec printed jumpsuit',
		'price':'54.20',
		'image':'product12.png',
		'sale':false,
		'entered':new Date('2017-01-09'),
		'featured':false,
		'items_sold':2
	},

];
