import { Injectable } from '@angular/core';
import { recipeData } from './mock-data'
import { Recipe } from './recipe';
import { Product } from './product';
import { products } from './products-data';

//import { Http } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/toPromise';


@Injectable()
export class DataService {

	//baseurl = 'http://mfd-recipe-api.herokuapp.com';
  baseurl = 'http://localhost/php/productsjson';

  constructor(private http: HttpClient) { }

  getRecipes(): Promise<Recipe[]> {
  	return this.http.get<Recipe[]>(`${this.baseurl}/recipes`)
  	.toPromise()
  	.catch(this.errorHandler);
  }

  private errorHandler(error) {
  	console.log(error);
  }

  getRecipesById(id): Recipe {
  	return recipeData.filter(recipe => recipe._id === id)[0];
  }

  getProducts(): Product[] {
    return products;
  }

  getProductById(id): Product {
    let temp = products.filter(function(product) {
        return product.id == id;
    });
    return temp[0];
  }

  getProducts2(): Promise<Product[]> {
    return this.http.get<Product[]>(`${this.baseurl}`)
    .toPromise()
    .catch(this.errorHandler);
  }


  /*
  getProducts(): Promise<Product[]> {
    return this.http.get<Product[]>(`${this.baseurl}/products.json`)
    .toPromise()
    .catch(this.errorHandler);    
  }
  */

  /*
    constructor() { }

  getRecipes(): Recipe[] {
  	return recipeData;
  }

  getRecipesById(id): Recipe {
  	return recipeData.filter(recipe => recipe._id === id)[0];
  }
*/

}
