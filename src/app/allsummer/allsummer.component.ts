import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-allsummer',
  templateUrl: './allsummer.component.html',
  styleUrls: ['./allsummer.component.scss']
})
export class AllsummerComponent implements OnInit {
	@Input() image:string;
	@Input() collection:string;

  constructor() { }

  ngOnInit() {
  }

}
