import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllsummerComponent } from './allsummer.component';

describe('AllsummerComponent', () => {
  let component: AllsummerComponent;
  let fixture: ComponentFixture<AllsummerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllsummerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllsummerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
