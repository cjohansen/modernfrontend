import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MusthaveComponent } from './musthave.component';

describe('MusthaveComponent', () => {
  let component: MusthaveComponent;
  let fixture: ComponentFixture<MusthaveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MusthaveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MusthaveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
