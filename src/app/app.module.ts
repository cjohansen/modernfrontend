import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { DataService } from './data.service'
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms'; // for template driven
import { ReactiveFormsModule } from '@angular/forms'; // for reactive driven


import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { SliderComponent } from './slider/slider.component';
import { LookbookComponent } from './lookbook/lookbook.component';
import { SubscribeComponent } from './subscribe/subscribe.component';
import { TopbrandsComponent } from './topbrands/topbrands.component';
import { ProductsComponent } from './products/products.component';
import { ReviewComponent } from './review/review.component';
import { SocialmediaComponent } from './socialmedia/socialmedia.component';
import { FooterComponent } from './footer/footer.component';
import { MusthaveComponent } from './musthave/musthave.component';
import { TesttestComponent } from './testtest/testtest.component';
import { SignupComponent } from './signup/signup.component';
import { AllsummerComponent } from './allsummer/allsummer.component';
import { HomeComponent } from './home/home.component';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import { LikebuttonComponent } from './likebutton/likebutton.component';
import { CountdownTimerComponent } from './countdown-timer/countdown-timer.component';
import { ProductComponent } from './product/product.component';
import { TemplateformComponent } from './templateform/templateform.component';
import { ReactiveformComponent } from './reactiveform/reactiveform.component';
import { CollectionComponent } from './collection/collection.component';
import { ContactComponent } from './contact/contact.component';
import { NavigationComponent } from './navigation/navigation.component';
import { ProductDescriptionComponent } from './product-description/product-description.component';
import { BagComponent } from './bag/bag.component';
import { SaleComponent } from './sale/sale.component';
import { SharpenyourlookComponent } from './sharpenyourlook/sharpenyourlook.component';
import { ExtraordinarydesignComponent } from './extraordinarydesign/extraordinarydesign.component';
import { SignupemailComponent } from './signupemail/signupemail.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SliderComponent,
    LookbookComponent,
    SubscribeComponent,
    TopbrandsComponent,
    ProductsComponent,
    ReviewComponent,
    SocialmediaComponent,
    FooterComponent,
    MusthaveComponent,
    TesttestComponent,
    SignupComponent,
    AllsummerComponent,
    HomeComponent,
    PagenotfoundComponent,
    LikebuttonComponent,
    CountdownTimerComponent,
    ProductComponent,
    TemplateformComponent,
    ReactiveformComponent,
    CollectionComponent,
    ContactComponent,
    NavigationComponent,
    ProductDescriptionComponent,
    BagComponent,
    SaleComponent,
    SharpenyourlookComponent,
    ExtraordinarydesignComponent,
    SignupemailComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    /*RouterModule.forRoot([
      { path:'signup', component:SignupComponent},
      { path:'recipes', component:TesttestComponent},
      { path:'recipe/:id', component:LookbookComponent},
      { path:'', redirectTo: 'recipes', pathMatch: 'full'},
      { path:'**', component:PagenotfoundComponent}
    ])*/
    RouterModule.forRoot([
      { path:'signup', component:SignupComponent},
      { path:'home', component:HomeComponent},
      { path:'collection', component:CollectionComponent},
      { path:'collection/:brand', component:CollectionComponent},
      { path:'contact', component:ContactComponent},
      { path:'product', component:ProductDescriptionComponent},
      { path:'product/:id', component:ProductDescriptionComponent},
      { path:'', redirectTo: 'home', pathMatch: 'full'},
      { path:'**', component:PagenotfoundComponent}
    ])
  ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
