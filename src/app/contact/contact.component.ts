import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ValidateEmail } from '../email-validator';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  contactForm: FormGroup;
  post:any;                     // A property for our submitted form
  message = '';
  name = '';
  email = '';

  constructor(private fb: FormBuilder) { 
    this.contactForm = fb.group({
      'name' : [null, Validators.required],
      'message' : [null, Validators.compose([Validators.required, Validators.minLength(30)])],
      'email': ['', [Validators.required, ValidateEmail]],
    });
  }

  validateName() {
    return !this.contactForm.controls['name'].valid && this.contactForm.controls['name'].touched;
  }

  validateEmail() {
    return !this.contactForm.controls['email'].valid && this.contactForm.controls['email'].touched;
  }

  validateMessage() {
    return !this.contactForm.controls['message'].valid && this.contactForm.controls['message'].touched;
  }

  ngOnInit() {
    /*this.contactForm.get('validate').valueChanges.subscribe(

      (validate) => {

          if (validate == '1') {
              this.contactForm.get('name').setValidators([Validators.required, Validators.minLength(3)]);
          } else {
              this.contactForm.get('name').setValidators(Validators.required);
          }
          this.contactForm.get('name').updateValueAndValidity();

      });*/
  }

  addPost(post) {
    //this.message = post.message;
    //this.name = post.name;
  }

}
