import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SharpenyourlookComponent } from './sharpenyourlook.component';

describe('SharpenyourlookComponent', () => {
  let component: SharpenyourlookComponent;
  let fixture: ComponentFixture<SharpenyourlookComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SharpenyourlookComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SharpenyourlookComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
