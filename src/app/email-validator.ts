import { AbstractControl } from '@angular/forms';

export function ValidateEmail(control: AbstractControl) {
    var regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    let test = regex.test(control.value);
    if ( !test ) {
    	return {validEmail:test};
    } 
    else {
    	return null;
    }
}


  /*if (!control.value.startsWith('https') || !control.value.includes('.io')) {
    return { validEmail: true };
  }
  return null;*/



