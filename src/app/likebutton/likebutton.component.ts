import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Recipe } from '../recipe';

@Component({
  selector: 'app-likebutton',
  templateUrl: './likebutton.component.html',
  styleUrls: ['./likebutton.component.scss']
})
export class LikebuttonComponent implements OnInit {
	@Input() likeCount: number;
	@Input() recipeId: String;
	@Input() recipe: Recipe;
	@Output() onButtonClicked = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
  }

  doSomething() {
  	//this.onButtonClicked.emit(this.recipeId+' I was clicked');
  	this.recipe.social_rank++;
  }

}
