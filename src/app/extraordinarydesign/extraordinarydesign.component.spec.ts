import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExtraordinarydesignComponent } from './extraordinarydesign.component';

describe('ExtraordinarydesignComponent', () => {
  let component: ExtraordinarydesignComponent;
  let fixture: ComponentFixture<ExtraordinarydesignComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExtraordinarydesignComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExtraordinarydesignComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
