import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-extraordinarydesign',
  templateUrl: './extraordinarydesign.component.html',
  styleUrls: ['./extraordinarydesign.component.scss']
})
export class ExtraordinarydesignComponent implements OnInit {
	@Input() direction;

  constructor() { }

  ngOnInit() {
  }

}
