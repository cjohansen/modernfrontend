import { Component, OnInit, Input } from '@angular/core';
import { CountdownTimerComponent } from "../countdown-timer/countdown-timer.component"
import { AfterViewInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements AfterViewInit,OnInit {
	@ViewChild(CountdownTimerComponent)  // bemærk ikke semikolon
  @Input() test: string;

	private timerComponent: CountdownTimerComponent;

	seconds = 0;

  constructor() { 
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
  	setTimeout(() => this.seconds = this.timerComponent.seconds,0);
  }

	start() {
		this.timerComponent.start();
	}

	stop() {
		this.timerComponent.stop();

	}

}


