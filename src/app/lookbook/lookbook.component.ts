import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataService } from '../data.service';
import { Recipe } from '../recipe';

@Component({
  selector: 'app-lookbook',
  templateUrl: './lookbook.component.html',
  styleUrls: ['./lookbook.component.scss']
})
export class LookbookComponent implements OnInit {

  recipe: Recipe;
  recipeid: String;

  constructor(private dataService: DataService, private route: ActivatedRoute) {

  }

  ngOnInit() {
  	this.recipeid = this.route.snapshot.paramMap.get('id');
  	this.recipe = this.dataService.getRecipesById(this.recipeid);
  }

}
