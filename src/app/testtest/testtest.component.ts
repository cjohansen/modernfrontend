import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { Recipe } from '../recipe';

@Component({
  selector: 'app-testtest',
  templateUrl: './testtest.component.html',
  styleUrls: ['./testtest.component.scss']
})
export class TesttestComponent implements OnInit {

	hvadskerder = "hvad sker der";

  recipes: Recipe[];

  constructor(private dataService: DataService) {

  }

  ngOnInit() {
    this.dataService.getRecipes()
    .then(recipes => this.recipes = recipes);

  	//this.recipes = this.dataService.getRecipes();
  }

  numberOfReviews(recipe) {
  	if ( typeof recipe.reviews != 'undefined' ) return recipe.reviews.length;
  	else return 0;
  }

  avgRating(recipe) {
  	if ( typeof recipe.reviews != 'undefined' ) {
	  	let avg_rating = 0;
	  	for (let review of recipe.reviews) {
	  		avg_rating += review.rating;
	  	}
	  	return avg_rating / recipe.reviews.length;
	}
	else return 0;
  }

  testclick(recipe) {
  	recipe.social_rank++;
  }

  allPublisher() {

  }

  logMessage(recipe) {
    console.log(recipe);
  }

}
