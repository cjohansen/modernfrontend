import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-templateform',
  templateUrl: './templateform.component.html',
  styleUrls: ['./templateform.component.scss']
})
export class TemplateformComponent implements OnInit {

	user: User;
	submitted = false;

  constructor() { }

  ngOnInit() {
  	this.user = new User();
  	this.user.firstname = 'Frank';
  	this.user.email = 'frank@work.dk';
  }

  submitForm() {
  	this.submitted = true;
  	console.log('hejsa');
  }

}


class User {
	firstname:string;
	email:string;
}
