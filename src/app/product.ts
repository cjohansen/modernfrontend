export class Product {
	public id:number;
	public description: string;
	public price: string;
	public image: string;
	public sale: boolean;
	public entered: Date;
	public featured: boolean;
	public items_sold: number;
}
