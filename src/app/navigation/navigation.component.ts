import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {
	@Input() look:string;
  @Input() items_in_basket:string;



	menuVisible = false;

  constructor() { 
    this.items_in_basket = 'empty';
  }

  ngOnInit() {
  }

  toggleMenu() {
  	this.menuVisible = !this.menuVisible;
  }

}
