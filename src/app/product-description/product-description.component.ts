import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataService } from '../data.service';
import { Product } from '../product';


@Component({
  selector: 'app-product-description',
  templateUrl: './product-description.component.html',
  styleUrls: ['./product-description.component.scss']
})
export class ProductDescriptionComponent implements OnInit {

	product:Product;
	productid:string;
  tab = "description";

  constructor(private dataService: DataService, private route: ActivatedRoute) { 
  }

  ngOnInit() {
  	this.productid = this.route.snapshot.paramMap.get('id');
  	this.product = this.dataService.getProductById(this.productid);  	
  }

}
